<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\User\CreateUserRequest;
use App\Http\Requests\User\UpdateUserRequest;
use App\Services\User\CreateUserService;
use App\Services\User\DeleteUserService;
use App\Services\User\FindUserService;
use App\Services\User\ListUserService;
use App\Http\Controllers\Controller;
use App\Services\User\UpdateUserService;

class UserController extends Controller
{
    protected $listUserService;

    protected $createUserService;

    protected $deleteUserService;

    protected $findUserService;

    protected $updateUserService;

    public function __construct(
        ListUserService $listUserService,
        CreateUserService $createUserService,
        DeleteUserService $deleteUserService,
        FindUserService $findUserService,
        UpdateUserService $updateUserService
    ) {
        $this->listUserService = $listUserService;
        $this->createUserService = $createUserService;
        $this->deleteUserService = $deleteUserService;
        $this->findUserService = $findUserService;
        $this->updateUserService = $updateUserService;
    }

    public function index()
    {
        $users = $this->listUserService->handle();

        return response()->success($users);
    }

    public function store(CreateUserRequest $request)
    {
        $user = $this->createUserService
            ->setRequest($request)
            ->handle();

        return response()->created($user);
    }

    public function show($id)
    {
        $user = $this->findUserService
            ->setModel($id)
            ->handle();

        return response()->success($user);
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $userUpdate = $this->updateUserService
            ->setRequest($request)
            ->setModel($id)
            ->handle();

        return response()->success($userUpdate);
    }

    public function destroy($id)
    {
        $this->deleteUserService->setModel($id)->handle();

        return response()->successWithoutData();
    }
}
