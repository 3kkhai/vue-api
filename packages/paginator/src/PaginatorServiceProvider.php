<?php

namespace Mi\Paginator;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\ServiceProvider;
use Mi\Paginator\CursorPaginator;
use Illuminate\Support\Arr;

class PaginatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     *
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function boot()
    {
        $macro = function ($perPage = null, $columns = ['*'], $options = []) {
            $perPage = $perPage ?: $this->model->getPerPage();

            // Handle next / prev cursor, prefer next_cursor
            $cursor = CursorPaginator::resolveNextCursor() ?: CursorPaginator::resolvePrevCursor();

            if (! ($options['ignore_cursor'] ?? false) && ! empty($cursor)) {
                $this->where(function ($query) use ($cursor) {
                    foreach ($cursor as $c) {
                        if ($c[0] != $this->model->getKeyName()) {
                            $query = call_user_func_array([$query, 'orWhere'], $c);

                            continue;
                        }

                        // because the primary key is unique, but another fields
                        // let check the primary key's condition in case of other fields with the same value
                        // eg: a > 1 and b > 2 or ((a = 1 or b = 1) and id > 3)
                        $query = $query->orWhere(function ($query) use ($c, $cursor) {
                            $query = call_user_func_array([$query, 'orWhere'], $c);

                            return $query->where(function ($query) use ($cursor) {
                                foreach ($cursor as $c1) {
                                    if ($c1[0] != $this->model->getKeyName()) {
                                        $query = call_user_func_array([$query, 'orWhere'], [$c1[0], '=', $c1[2]]);
                                    }
                                }

                                return $query;
                            });
                        });
                    }

                    return $query;
                });
            }

            // Force order by primary key if this order is not exists
            $primaryKey = $this->model->getKeyName();
            $orderByPrimaryKey = Arr::first($this->query->orders ?: [], function ($value) use ($primaryKey) {
                return (isset($value['column']) && $value['column'] == $primaryKey)
                    || (isset($value['type']) && $value['type'] == 'Raw' && preg_match('/^'. $primaryKey .'\s+/', $value['sql']));
            });

            if (is_null($orderByPrimaryKey)) {
                $this->orderBy($this->model->getKeyName());
            }

            $this->take($perPage + 1);

            return new CursorPaginator($this->get($columns), $perPage, [
                'orders' => $this->query->orders
            ]);
        };

        QueryBuilder::macro('cursorPaginate', $macro);
        EloquentBuilder::macro('cursorPaginate', $macro);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->resolveCursorParametersFromRequest();
    }

    /**
     * Resolve cursor parameters from request by picking next_cursor and prev_cursor
     *
     * @return void
     */
    private function resolveCursorParametersFromRequest()
    {
        CursorPaginator::nextCursorResolver(function ($cursor = 'next_cursor') {
            $cursor = $this->app['request']->input($cursor);

            if (isset($cursor) && ($parsed = json_decode(base64_decode($cursor, true), true))) {
                return $parsed;
            }

            return null;
        });

        CursorPaginator::prevCursorResolver(function ($cursor = 'prev_cursor') {
            $cursor = $this->app['request']->input($cursor);

            if (isset($cursor) && ($parsed = json_decode(base64_decode($cursor, true), true))) {
                return $parsed;
            }

            return null;
        });
    }
}
