<?php

namespace Mi\Paginator;

use ArrayAccess;
use Closure;
use Countable;
use Illuminate\Contracts\Pagination\Paginator as PaginatorContract;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Support\Collection;
use IteratorAggregate;
use JsonSerializable;

class CursorPaginator extends AbstractPaginator implements Arrayable, ArrayAccess, Countable, IteratorAggregate, JsonSerializable, Jsonable, PaginatorContract
{
    /**
     * Determine if there are more items in the data source.
     *
     * @return bool
     */
    protected $hasMore = false;

    /**
     * @var array
     */
    protected $orders;

    /**
     * @var \Closure
     */
    protected static $nextCursorResolver;

    /**
     * @var \Closure
     */
    protected static $prevCursorResolver;

    /**
     * Create a new paginator instance.
     *
     * @param mixed $items
     * @param int $perPage
     * @param array $options
     */
    public function __construct($items, $perPage, $options = [])
    {
        $this->orders  = $options['orders'] ?? [];
        $this->perPage = $perPage;

        $this->setItems($items);
    }

    private function setItems($items)
    {
        $items = $items instanceof Collection ? $items : new Collection($items);

        $this->hasMore = $items->count() > $this->perPage;
        $this->items   = $items->splice(0, $this->perPage);
    }

    /**
     * Get the next cursor
     *
     * @return string|null
     */
    public function nextCursor()
    {
        if (! $this->hasMore) {
            return null;
        }

        $lastItem = $this->items->last();

        // we will have 2 ways of order
        // 1. order by column => ["column" => "start_at", "direction" => "start_at"]
        // 2. order by raw => ["type" => "Raw", "sql" => "start_at DESC NULLS LAST"]
        $cursors = [];
        $hasPrimaryKeyOrder = false;

        foreach ($this->orders as $order) {
            $column    = $order['column'] ?? null;
            $direction = $order['direction'] ?? null;

            // detect order from raw query
            if (isset($order['type']) && preg_match('/^([\w\.]+)\s+(asc|desc).*$/i', $order['sql'], $found)) {
                $column = $found[1];
                $direction = $found[2];
            }

            if (is_null($column) || is_null($direction)) {
                continue;
            }

            if ($column == $lastItem->getKeyName()) {
                $hasPrimaryKeyOrder = true;
            }

            $operator = (strtolower($direction) == 'desc') ? '<' : '>';
            $cursors[] = [$column, $operator, $lastItem->$column];
        }

        if (! $hasPrimaryKeyOrder) {
            $cursors[] = [$lastItem->getKeyName(), '>', $lastItem->getAttribute($lastItem->getKeyName())];
        }

        return base64_encode(json_encode($cursors));
    }

    /**
     * Get the prev cursor
     *
     * @return string|null
     */
    public function prevCursor()
    {
        // TODO: add more logic
        return 'TODO';
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        // TODO: not use anywhere in the current
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param  int  $options
     * @return string
     */
    public function toJson($options = 0)
    {
        \Log::debug('package:paginator:toJson', [$options]);
        // TODO: not use anywhere in the current
    }

    /**
     * The URL for the next page, or null.
     *
     * @return string|null
     */
    public function nextPageUrl()
    {
        // TODO: not use anywhere in the current
    }

    /**
     * Determine if there is more items in the data store.
     *
     * @return bool
     */
    public function hasMorePages()
    {
        return $this->hasMore;
    }

    /**
     * Specify data which should be serialized to JSON.
     *
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     *
     * @return mixed data which can be serialized by <b>json_encode</b>,
     *               which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        // TODO: not use anywhere in the current
    }

    /**
     * Render the paginator using a given view.
     *
     * @param  string|null  $view
     * @param  array  $data
     * @return string
     */
    public function render($view = null, $data = [])
    {
        \Log::debug('package:paginator:render', [$view, $data]);
        // TODO: not use anywhere in the current
    }

    /**
     * Set the next cursor resolver callback.
     *
     * @param  \Closure  $resolver
     * @return void
     */
    public static function nextCursorResolver(Closure $resolver)
    {
        static::$nextCursorResolver = $resolver;
    }

    /**
     * Set the prev cursor resolver callback.
     *
     * @param  \Closure  $resolver
     * @return void
     */
    public static function prevCursorResolver(Closure $resolver)
    {
        static::$prevCursorResolver = $resolver;
    }

    /**
     * Resolve the next cursor
     *
     * @return array
     */
    public static function resolveNextCursor()
    {
        if (isset(static::$nextCursorResolver)) {
            return call_user_func(static::$nextCursorResolver);
        }

        return [];
    }

    /**
     * Resolve the prev cursor
     *
     * @return array
     */
    public static function resolvePrevCursor()
    {
        if (isset(static::$prevCursorResolver)) {
            return call_user_func(static::$prevCursorResolver);
        }

        return [];
    }
}
